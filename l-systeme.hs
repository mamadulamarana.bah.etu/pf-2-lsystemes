import Graphics.Gloss

type Symbole  = Char
type Mot      = [Symbole]
type Axiome   = Mot
type Regles   = Symbole -> Mot
type LSysteme = [Mot]

type EtatTortue = (Point, Float)

type Config = (EtatTortue -- État initial de la tortue
              ,Float      -- Longueur initiale d’un pas
              ,Float      -- Facteur d’échelle
              ,Float      -- Angle pour les rotations de la tortue
              ,[Symbole]) -- Liste des symboles compris par la tortue

type EtatDessin = (EtatTortue, Path)

motSuivant :: Regles -> Mot -> Mot
motSuivant _ [] = []
motSuivant r (x:xs) = r x ++ motSuivant r xs

motSuivant' :: Regles -> Mot -> Mot
motSuivant' r m = concat[r x | x <- m]

regle :: Regles
regle 'F' = "F-F++F-F"
regle '+' = "+"
regle '-' = "-"


axiome :: Axiome 
axiome = "F"

lsysteme :: Axiome -> Regles -> LSysteme
lsysteme ax r = a : lsysteme a r
    where a = motSuivant r ax

etatInitial :: Config -> EtatTortue
etatInitial (x,_,_,_,_) = x

longueurPas :: Config -> Float
longueurPas (_,x,_,_,_) = x

facteurEchelle :: Config -> Float
facteurEchelle (_,_,x,_,_) = x

angle :: Config -> Float
angle (_,_,_,x,_) = x

symbolesTortue :: Config -> [Symbole]
symbolesTortue (_,_,_,_,x) = x

avance :: Config -> EtatTortue -> EtatTortue
avance (_,d,_,_,_) ((x, y), cap) = ( (x + (d*cos(cap)) , y + (d*sin(cap))), cap )
    --( (fst(p) + (d*cos(cap)) , snd(p) + (d*sin(cap))), cap )

tourneAGauche :: Config -> EtatTortue -> EtatTortue
tourneAGauche (_,_,_,r,_) ((x,y), cap) = ( (x,y), cap + r )

tourneADroite :: Config -> EtatTortue -> EtatTortue
tourneADroite (_,_,_,r,_) ((x,y), cap) = ( (x,y), cap - r )

filtreSymboleTortue :: Config -> Mot -> Mot
filtreSymboleTortue (_,_,_,_,s) xs = filter (\ x -> elem x s ) xs 

interpreteSymbole :: Config -> EtatDessin -> Symbole -> EtatDessin
interpreteSymbole config (et, pts) s = (etat s, pts : [fst(etat s)])
    where etat s | s == '+' = tourneAGauche config et
                 | s == '-' = tourneADroite config et
                 | s == 'F' = avance config et


interpreteMot :: Config -> Mot -> Picture
interpreteMot config m = 

interpreteMot :: Config -> Mot -> Picture
interpreteMot config m = let (_, finalPath) = interpreteMot' (etatInitial config, []) mot in line finalPath
    where
        interpreteMot' :: EtatDessin -> Mot -> EtatDessin
        interpreteMot' etat [] = etat
        interpreteMot' etat (symbole:rest) =
            let newEtat = interpreteSymbole config etat symbole
            in interpreteMot' newEtat rest

